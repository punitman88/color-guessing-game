var totalColors = 6;
var colors = generateRandomColors(totalColors);

// Setting the text of current color on webpage
var currentColor = pickRandomColor();
var currentColorTextSpan = document.getElementById("currentColor");
currentColorTextSpan.textContent = currentColor;

var boxes = document.querySelectorAll(".box");
var displayMessage = document.getElementById("displaymessage");
var h1 = document.querySelector("h1");
var resetGameButton = document.getElementById("reset");

var easyBtn = document.querySelector("#easyBtn");
var hardBtn = document.querySelector("#hardBtn");

easyBtn.addEventListener("click", function(){
	easyBtn.classList.add("selected");
	hardBtn.classList.remove("selected");
	totalColors = 3;
	colors = generateRandomColors(totalColors);
	currentColor = pickRandomColor();
	currentColorTextSpan.textContent = currentColor;
	h1.style.backgroundColor = "#232323";
	for(var i=0; i<boxes.length; i++) {
		if(colors[i]) {
			boxes[i].style.backgroundColor = colors[i];
		} else {
			boxes[i].style.display = "none";
		}
	}
});

hardBtn.addEventListener("click", function(){
	hardBtn.classList.add("selected");
	easyBtn.classList.remove("selected");
	totalColors = 6;
	colors = generateRandomColors(totalColors);
	currentColor = pickRandomColor();
	currentColorTextSpan.textContent = currentColor;
	h1.style.backgroundColor = "#232323";
	for(var i=0; i<boxes.length; i++) {
		if(colors[i]) {
			boxes[i].style.backgroundColor = colors[i];
			boxes[i].style.display = "block";
		}
	}
});

resetGameButton.addEventListener("click", function(){
	colors = generateRandomColors(totalColors);
	currentColor = pickRandomColor();
	currentColorTextSpan.textContent = currentColor;
	for(var i=0; i<boxes.length; i++) {
		boxes[i].style.backgroundColor = colors[i];
	}
	displayMessage.textContent = "";
	resetGameButton.textContent = "Reset Game";
	h1.style.backgroundColor = "#232323";
});

for(var i=0; i<boxes.length; i++) {
	boxes[i].style.backgroundColor = colors[i];
	boxes[i].addEventListener("click", function(){
		var userPickedColor = this.style.backgroundColor;
		if(userPickedColor === currentColor) {
			displayMessage.textContent = "You're correct";
			changeColor(currentColor);
			h1.style.backgroundColor = currentColor;
			resetGameButton.textContent = "Play Again"
		} else {
			this.style.backgroundColor = "#232323";
			displayMessage.textContent = "Try Again";
		}
	});
}

function changeColor(color) {
	for(var i=0; i<boxes.length; i++) {
		boxes[i].style.backgroundColor = color;
	}
}

function pickRandomColor() {
	var randomColor = Math.floor(Math.random() * colors.length);
	return colors[randomColor];
}

function generateRandomColors(count) {
	var generatedRandomColors = [];
	for(var i=0; i<count; i++) {
		var r = randomNumberGenerator();
		var g = randomNumberGenerator();
		var b = randomNumberGenerator();
		var currentGeneratedColor = "rgb(" + r + ", " + g + ", " + b +")";
		generatedRandomColors.push(currentGeneratedColor);
	}
	return generatedRandomColors;
}

function randomNumberGenerator() {
	return Math.floor(Math.random() * 256);
}